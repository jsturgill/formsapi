<?php
$pass = 'whoopsnossl';
$user = 'fa-test-garbage@mailinator.com';
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Forms API</title>

	<link href="<?=asset('/css/app.css');?>" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <script>
        escapeHtml = function(str) {
            var div = document.createElement('div');
            div.appendChild(document.createTextNode(str));
            return div.innerHTML;
        };
    </script>
</head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Forms API</h1>
                    <p>The forms API is an experiment put together in just a few
                       days.  There are two resources/endpoints: fields and 
                       forms.  It is a work in progress.</p>
                        <ul>
                            <li><code><?=$url = url('api/v.9/fields');;?></code>
                            <li><code><?=$url = url('api/v.9/forms');?></code>
                        </ul>
                    <p>The API currently relies on HTTP Basic Authentication
                    over SSL.</p>
                    <p><strong>The API is a toy for now, and maybe forever.  Don't treat it otherwise.</strong></p>
                    <p>While the API was created for fun and learning, it is at least 
                       <em>theoretically</em> useful. Fields can be stored and 
                       retrieved via arbitrary labels, and collections of fields 
                       can be attached to a form.  This allows for the 
                       composition of forms, building them from a library of 
                       standard fields. It also allows for diverse forms to be 
                       kept up-to-date and synchronized easily: the 
                       "confirm-password" field could be modified once, for example, and that 
                       change would be reflected by every form that used the 
                       field.  When v1 arrives, it may actually be helpful to
                       someone, somewhere.</p>
                    <p>Below are a series of example requests.  The "Request" 
                       box contains jQuery code that your browser is (hopefully)
                       actually executing right now.  The other boxes are 
                       dynamically filled with the results.  This is a simple
                       way to both test and demonstrate the API.  The credentials
                       used are visible in the code snippet.  Feel free to play
                       around.</p>
                    <p>You can create your own account 
                    <a href="https://formsapi.jeremiahsturgill.com/auth/register">here</a>.
                    Use your email address as your username for HTTP Basic Authentication.</p>
                    <p>Unfortunately, I don't have a client yet for the API. You
                       can use cURL or <a title="Guzzle HTTP Client" 
                       href="https://github.com/guzzle/guzzle">Guzzle</a> to
                       interact with the API right now on your own domains, 
                       albeit somewhat tediously.</p>
                    <p>Implemented:</p>
                    <ul>
                        <li>HTTP Basic Authentication on all resources and requests</li>
                        <li>HTTPS Redirects (for now; v1 will refuse non-https 
                        without redirecting them)</li>
                        <li>Validation on all requests, with semantic HTTP error codes
                        such as <code>422 Unprocessable Entity</code> when invalid
                        information is submitted, or <code>404 Not Found</code> 
                        if a named resource does not exist.</li>
                        <li><code>GET</code>, <code>POST</code>, <code>PUT</code> (idempotent), 
                        <code>PATCH</code>, and <code>DELETE</code> for all resources</li>
                        <li>Many-to-many database schema, interfaced through Laravel's
                        Eloquent ORM.</li>
                        <li>Semantic Versioning</li>
                    </ul>
                    <p>Not yet implemented:</p>
                    <ul>
                        <li>Documentation</li>
                        <li>Testing</li>
                        <li>OAuth authentication</li>
                        <li>A stable API (version 1)</li>
                    </ul>
                </div>
            </div>
            <?=view('tests.1', ['pass' => $pass, 'user' => $user]);?>
            <?=view('tests.2', ['pass' => $pass, 'user' => $user]);?>
            <?=view('tests.3', ['pass' => $pass, 'user' => $user]);?>
            <?=view('tests.4', ['pass' => $pass, 'user' => $user]);?>
            <?=view('tests.5', ['pass' => $pass, 'user' => $user]);?>
            <?=view('tests.6', ['pass' => $pass, 'user' => $user]);?>

        </div>
        <!-- Scripts -->
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    	<script>
        	test_1();
    	</script>
    </body>
</html>
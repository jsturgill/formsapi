<div class="row"><?php
$url = url('api/v.9/fields');
ob_start();
?>$.ajax({
    "url": "<?=Html::entities($url);?>",
    "complete": function(jqXHR, status){
        $('#test_2_status').empty().append('<code>' + jqXHR.status + ' ' + jqXHR.statusText + '</code>');
        test_3(); // call next test (no prep)
    },
    "error": function(jqXHR, status, error) {
        $('#test_2_response').empty().append(JSON.stringify(jqXHR.responseJSON,null, 2));
    },
    "success": function(data, status, jqXHR) {
        $('#test_2_html').empty().append(data.html);
        $('#test_2_response').empty().append(escapeHtml(JSON.stringify(jqXHR.responseJSON, null, 2)));
    },
    "password": '<?=$pass;?>',
    "username": '<?=$user;?>',
    "type": "POST",
    "data": {
        "field": "input",
        "name" : "awesome_field",
        "attributes" : {
            "type" : "checkbox",
            "value": "hello world",
            "class" : ["test", "test2"],
            "id" : "whatever",
            "selected" : "selected",
            "name" : "field_name",
            "data" : {
                "some_data_field" : "some value here"
            }
        },
        "label": {
            "attributes": {
                "style": {
                    "font-weight" : "bold"
                }
            },
            "placement" : "wrap",
            "pre_field_content" : "Checkbox Label: ",
            "post_field_content" : ""
        },
        "before" : "<div class='example_container'>",
        "after" : "</div>"
    }
});
<?php
$js = ob_get_clean();
?>
<div class="col-md-12">
<h2>Named Field POST Example</h2>
<p>Below is an example <code>POST</code> request to <code><?=$url;?></code>.  The API takes 
the submitted configuration, stores it for future use, and returns (on success)
the folllwing information:</p>
<ul>
    <li><code>name</code>: the name of the field</li>
    <li><code>config</code>: the configuration of the field</li>
    <li><code>html</code>: the rendered field</li>
    <li><code>status</code>: success
</ul>
</div>
<div class="col-md-6">
    <h3>Request</h3>
    <pre><code>
    <?=Html::entities($js);?>
    </code></pre>
</div>
<div class="col-md-6">
    <h3>Response</h3>
    <pre><code id="test_2_response"><i class="fa fa-spinner fa-spin"></i></code></pre>
    <p id="test_2_status">&nbsp;</p>
    <h4>Rendered HTML</h4>
    <pre><code id="test_2_html"><i class="fa fa-spinner fa-spin"></i></code></pre>
</div>
<script>
    function test_2() {
        <?=$js;?>
    }
</script>
</div>
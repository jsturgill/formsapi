    <h1 id="post_input_test">Post Input Test</h1>
    <script>
        $.ajax({
            url: "http://formsapi.jeremiahsturgill.com/api/v1/fields",
            complete: function(jqXHR, status){
                console.log('complete function called');
            },
            error: function(jqXHR, status, error) {
                console.log('error function called');
                $('#post_input_test').after(JSON.stringify(jqXHR.responseJSON));
            },
            success: function(data, status, jqXHR) {
                $('#post_input_test').after(data.html);
                console.log('success function called');
            },
            password: 'whoopsnossl',
            username: 'fa-test-garbage@mailinator.com',
            type: "POST",
            data: {
                "field" : "input",
                "name" : "test26",
                "before" : "<div class='container'>",
                "after" : "</div>"
            },
        });
    </script>
</div>
<h1 id="named_input_test">Named Input Test</h1>
    <script>
        $.ajax({
            url: "http://formsapi.jeremiahsturgill.com/api/v1/fields/formtest3",
            complete: function(jqXHR, status){
                console.log('complete function called');
            },
            error: function(jqXHR, status, error) {
                $('#named_input_test').after(JSON.stringify(jqXHR.responseJSON));
                console.log('error function called');
            },
            success: function(data, status, jqXHR) {
                $('#named_input_test').after(data.html);
                console.log('success function called');
            },
            password: 'whoopsnossl',
            username: 'fa-test-garbage@mailinator.com',
            type: "GET",
            data: {
                "name" : "formtest3",
                "before" : "<div class='container'>",
                "after" : "</div>"
            },
        });
    </script>
</div>
<div>
    <h1 id="input_test">Input Test</h1>
    <script>
        $.ajax();
    </script>
</div>
<div>
    <h1 id="textarea_test">Textarea Test</h1>
    <script>
        $.ajax({
            url: "http://formsapi.jeremiahsturgill.com/api/v1/fields",
            complete: function(jqXHR, status){
                console.log('complete function called');
            },
            error: function(jqXHR, status, error) {
                console.log('error function called');
                $('#textarea_test').after(JSON.stringify(jqXHR.responseJSON));
            },
            success: function(data, status, jqXHR) {
                $('#textarea_test').after(data.html);
                console.log('success function called');
            },
            password: 'whoopsnossl',
            username: 'fa-test-garbage@mailinator.com',
            type: "GET",
            data: {
                "field": "textarea",
                "content" : "hello world",
                "attributes" : {
                    "id": "whatever2",
                    "class" : ["test", "test2"],
                    "data" : {
                        "some_data_field" : "some value here"
                    }
                },
                "before" : "<div class='container'>",
                "after" : "</div>"
            },
        });
    </script>
    <h1 id="select_test">Select Test</h1>
    <script>
        $.ajax({
            url: "http://formsapi.jeremiahsturgill.com/api/v1/fields",
            complete: function(jqXHR, status){
                console.log('complete function called');
            },
            error: function(jqXHR, status, error) {
                console.log('error function called');
                $('#select_test').after(JSON.stringify(jqXHR.responseJSON));
            },
            success: function(data, status, jqXHR) {
                $('#select_test').after(data.html);
                console.log('success function called');
            },
            type: "GET",
            password: 'whoopsnossl',
            username: 'fa-test-garbage@mailinator.com',
            data: {
               "field":"select",
               
               "attributes":{
                    "id":"whatever3",
                    "class":[
                        "test",
                        "test2"
                    ],
                    "data":{
                        "some_data_field":"some value here"
                    }
               },
               "child_fields":[
                    {
                        "field":"option",
                        "content":"option text here",
                        "attributes":{
                            "value":"opt_1"
                        }
                    },
                    {
                        "field":"optgroup",
                        "attributes":{
                            "label":"begin group!"
                        },
                        "child_fields":[
                            {
                               "field":"option",
                               "content":"more options",
                               "attributes":{
                                  "value":"opt_2"
                               }
                            }
                        ]
                    },
                    {
                        "field":"option",
                        "content":"third option",
                        "attributes":{
                            "value":"opt_3"
                        }
                    }
               ],
               "before":"<div class='container'>",
               "after":"</div>"
            },
        });
    </script>
    <h1 id="fieldset_test">Fieldset Test</h1>
    <script>
        $.ajax({
            url: "http://formsapi.jeremiahsturgill.com/api/v1/fields",
            complete: function(jqXHR, status){
                console.log('complete function called');
            },
            error: function(jqXHR, status, error) {
                console.log('error function called');
                $('#fieldset_test').after(JSON.stringify(jqXHR.responseJSON));
            },
            success: function(data, status, jqXHR) {
                $('#fieldset_test').after(data.html);
                console.log('success function called');
            },
            type: "GET",
            password: 'whoopsnossl',
            username: 'fa-test-garbage@mailinator.com',
            data: {
               "field":"fieldset",
               "attributes":{
                    "id":"whatever3",
                    "class":[
                        "test",
                        "test2"
                    ],
                    "data":{
                        "some_data_field":"some value here"
                    }
               },
               "child_fields":[
                    {
                        "field":"legend",
                        "attributes":{
                            "data": {
                                "test" : "some value"
                            }
                        },
                        "content" : "here is the legend"
                    },
                    {
                        "field":"input",
                        "attributes":{
                            "type":"checkbox",
                            "value":"opt_1",
                            "name": "checkbox_test"
                        },
                        "label": {
                            "attributes": {
                                "style": {
                                    "color" : "red"
                                }
                            },
                            "placement" : "wrap",
                            "pre_field_content" : "Checkbox label ",
                            "post_field_content" : " is here!"
                        }
                    },
                    {
                        "field":"input",
                        "attributes":{
                            "type":"text",
                            "value": "fieldset input test value"
                        },
                        "label" : {
                            "attributes" : {
                                "style" : {
                                    "color" : "blue"
                                }
                            },
                            "placement" : "before",
                            "content" : "Test Label Here (before)"
                        }
                    }
               ],
               "before":"<div class='container'>",
               "after":"</div>"
            },
        });
    </script>
</div>
<div>
    <h1 id="form_delete_test">Form Delete Test</h1>
    <script>
        $.ajax({
            url: "http://formsapi.jeremiahsturgill.com/api/v1/forms/testform1",
            complete: function(jqXHR, status){
                console.log('complete function called');
            },
            error: function(jqXHR, status, error) {
                console.log('error function called');
                $('#form_delete_test').after(JSON.stringify(jqXHR.responseJSON));
            },
            success: function(data, status, jqXHR) {
                $('#form_delete_test').after(data.html);
                console.log('success function called');
            },
            type: "DELETE",
            password: 'whoopsnossl',
            username: 'fa-test-garbage@mailinator.com',
            data: {
                "name" : "testform1",
            },
        });
    </script>
</div>
<div>
    <h1 id="form_post_test">Form Post Test</h1>
    <script>
        $.ajax({
            url: "http://formsapi.jeremiahsturgill.com/api/v1/forms",
            complete: function(jqXHR, status){
                console.log('complete function called');
            },
            error: function(jqXHR, status, error) {
                console.log('error function called');
                $('#form_post_test').after(JSON.stringify(jqXHR.responseJSON));
            },
            success: function(data, status, jqXHR) {
                $('#form_post_test').after(data.html);
                console.log('success function called');
            },
            type: "POST",
            password: 'whoopsnossl',
            username: 'fa-test-garbage@mailinator.com',
            data: {
                "name" : "testform1",
                "attributes":{
                    "id":"whatever3",
                    "class":[
                        "test",
                        "test2"
                    ],
                    "data":{
                        "some_data_field":"some value here"
                    },
                    "method" : "post",
                    "action" : "test"
                },
                "child_fields":[
                    {
                        "field":"input",
                        "name" : "formtest1",
                        "attributes":{
                            "data": {
                                "test" : "some value"
                            },
                            "value" : "some input",
                            "name" : "form_test"
                        }
                    },
                    {
                        "field":"input",
                        "name" : "formtest2",
                        "attributes":{
                            "type":"checkbox",
                            "value":"opt_1",
                            "name": "checkbox_test"
                        },
                        "label": {
                            "attributes": {
                                "style": {
                                    "color" : "red"
                                }
                            },
                            "placement" : "wrap",
                            "pre_field_content" : "Checkbox label ",
                            "post_field_content" : " is here!"
                        }
                    },
                    {
                        "field":"input",
                        "name" : "formtest3",
                        "attributes":{
                            "type":"text",
                            "value": "fieldset input test value"
                        },
                        "label" : {
                            "attributes" : {
                                "style" : {
                                    "color" : "blue"
                                }
                            },
                            "placement" : "before",
                            "content" : "Test Label Here (before)"
                        }
                    }
                ],
               "before":"<div class='container'>",
               "after":"</div>"
            },
        });
    </script>
</div>
<div>
    <h1 id="form_put_test">Form Put/Patch Test</h1>
    <script>
        $.ajax({
            url: "http://formsapi.jeremiahsturgill.com/api/v1/forms/testform1",
            complete: function(jqXHR, status){
                console.log('complete function called');
            },
            error: function(jqXHR, status, error) {
                console.log('error function called');
                $('#form_put_test').after(JSON.stringify(jqXHR.responseJSON));
            },
            success: function(data, status, jqXHR) {
                $('#form_put_test').after(data.html);
                console.log('success function called');
            },
            type: "PATCH",
            password: 'whoopsnossl',
            username: 'fa-test-garbage@mailinator.com',
            data: {
                "name" : "testform1",
                "attributes":{
                    "id":"whatever3",
                    "class":[
                        "test",
                        "test2"
                    ],
                    "data":{
                        "some_data_field":"some value here"
                    },
                    "method" : "post",
                    "action" : "test"
                },
                "child_fields":[
                    {
                        "name" : "formtest1",
                    },
                    {
                        "name" : "formtest3",
                    },
                    {
                        "name" : "formtest2",
                    }
                ],
               "before":"<div class='container'><p>CRAZY!</p>",
               "after":"</div>"
            },
        });
    </script>
</div>
<div>
    <h1 id="form_show_test">Form Show Test</h1>
    <script>
        $.ajax({
            url: "http://formsapi.jeremiahsturgill.com/api/v1/forms/testform1",
            complete: function(jqXHR, status){
                console.log('complete function called');
            },
            error: function(jqXHR, status, error) {
                console.log('error function called');
                $('#form_show_test').after(JSON.stringify(jqXHR.responseJSON));
            },
            success: function(data, status, jqXHR) {
                $('#form_show_test').after(data.html);
                console.log('success function called');
            },
            type: "GET",
            password: 'whoopsnossl',
            username: 'fa-test-garbage@mailinator.com',
            data: {
                "name" : "testform1",
                "attributes":{
                    "id":"whatever3",
                    "class":[
                        "test",
                        "test2"
                    ],
                    "data":{
                        "some_data_field":"some value here"
                    },
                    "method" : "post",
                    "action" : "test"
                },
                "child_fields":[
                    {
                        "field":"input",
                        "name" : "formtest1",
                        "attributes":{
                            "data": {
                                "test" : "some value"
                            },
                            "value" : "some input",
                            "name" : "form_test"
                        }
                    },
                    {
                        "field":"input",
                        "name" : "formtest2",
                        "attributes":{
                            "type":"checkbox",
                            "value":"opt_1",
                            "name": "checkbox_test"
                        },
                        "label": {
                            "attributes": {
                                "style": {
                                    "color" : "red"
                                }
                            },
                            "placement" : "wrap",
                            "pre_field_content" : "Checkbox label ",
                            "post_field_content" : " is here!"
                        }
                    },
                    {
                        "field":"input",
                        "name" : "formtest3",
                        "attributes":{
                            "type":"text",
                            "value": "fieldset input test value"
                        },
                        "label" : {
                            "attributes" : {
                                "style" : {
                                    "color" : "blue"
                                }
                            },
                            "placement" : "before",
                            "content" : "Test Label Here (before)"
                        }
                    }
                ],
               "before":"<div class='container'>",
               "after":"</div>"
            },
        });
    </script>
</div>
<div class="row"><?php
$url = url('api/v.9/fields') . '/awesome_field';
ob_start();
?>$.ajax({
    "url": "<?=Html::entities($url);?>",
    "complete": function(jqXHR, status){
        $('#test_3_status').empty().append('<code>' + jqXHR.status + ' ' + jqXHR.statusText + '</code>');
        test_4(); // call next test (no prep)
    },
    "error": function(jqXHR, status, error) {
        $('#test_3_response').empty().append(JSON.stringify(jqXHR.responseJSON, null, 2));
    },
    "success": function(data, status, jqXHR) {
        $('#test_3_html').empty().append(data.html);
        $('#test_3_response').empty().append(escapeHtml(JSON.stringify(jqXHR.responseJSON, null, 2)));
    },
    "password": '<?=$pass;?>',
    "username": '<?=$user;?>',
    "type": "GET",
    "data": {
        "name" : "awesome_field",
    }
});
<?php
$js = ob_get_clean();
?>
<div class="col-md-12">
<h2>Named Field GET Example</h2>
<p>Below is an example <code>GET</code> request to <code><?=$url;?></code>.  The API returns
the previously-saved field on success.</p>
<p>The same URI would also accept the <code>DELETE</code> verb.  When acting on 
a named resource, the name has to be present in the submitted parameters as well 
as in the URI.  You can see that in this request.</p>
</div>
<div class="col-md-6">
    <h3>Request</h3>
    <pre><code>
    <?=Html::entities($js);?>
    </code></pre>
</div>
<div class="col-md-6">
    <h3>Response</h3>
    <pre><code id="test_3_response"><i class="fa fa-spinner fa-spin"></i></code></pre>
    <p id="test_3_status"></p>
    <h4>Rendered HTML</h4>
    <pre><code id="test_3_html"><i class="fa fa-spinner fa-spin"></i></code></pre>
</div>
<script>
    function test_3() {
        <?=$js;?>
    }
</script>
</div>
<div class="row"><?php
$url = url('api/v.9/fields') . '/awesome_field';
ob_start();
?>$.ajax({
    "url": "<?=Html::entities($url);?>",
    "complete": function(jqXHR, status){
        $('#test_5_status').empty().append('<code>' + jqXHR.status + ' ' + jqXHR.statusText + '</code>');
        test_6(); // call next test (no prep)
    },
    "error": function(jqXHR, status, error) {
        $('#test_5_response').empty().append(JSON.stringify(jqXHR.responseJSON, null, 2));
    },
    "success": function(data, status, jqXHR) {
        $('#test_5_html').empty().append(data.html);
        $('#test_5_response').empty().append(escapeHtml(JSON.stringify(jqXHR.responseJSON, null, 2)));
    },
    "password": '<?=$pass;?>',
    "username": '<?=$user;?>',
    "type": "DELETE",
    "data": {
        "name" : "awesome_field",
        "attributes" : {
            "style" : {
                "color" : "blue"
            }
        }
    }
});
<?php
$js = ob_get_clean();
?>
<div class="col-md-12">
<h2>Named Field DELETE Example</h2>
<p>Below is an example <code>DELETE</code> request to <code><?=$url;?></code>. The deleted
field's configuration and HTML rendering are included in the response.  Note that attributes
are defined in the request.  Because it is a <code>DELETE</code> request, they serve no
purpose.  Their redundant presence does not cause an error.</p>
</div>
<div class="col-md-6">
    <h3>Request</h3>
    <pre><code>
    <?=Html::entities($js);?>
    </code></pre>
</div>
<div class="col-md-6">
    <h3>Response</h3>
    <pre><code id="test_5_response"><i class="fa fa-spinner fa-spin"></i></code></pre>
    <p id="test_5_status"></p>
    <h4>Rendered HTML</h4>
    <pre><code id="test_5_html"><i class="fa fa-spinner fa-spin"></i></code></pre>
</div>
<script>
    function test_5() {
        <?=$js;?>
    }
</script>
</div>
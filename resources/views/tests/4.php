<div class="row"><?php
$url = url('api/v.9/fields') . '/awesome_field';
ob_start();
?>$.ajax({
    "url": "<?=Html::entities($url);?>",
    "complete": function(jqXHR, status){
        $('#test_4_status').empty().append('<code>' + jqXHR.status + ' ' + jqXHR.statusText + '</code>');
        test_5(); // call next test (no prep)
    },
    "error": function(jqXHR, status, error) {
        $('#test_4_response').empty().append(JSON.stringify(jqXHR.responseJSON, null, 2));
    },
    "success": function(data, status, jqXHR) {
        $('#test_4_html').empty().append(data.html);
        $('#test_4_response').empty().append(escapeHtml(JSON.stringify(jqXHR.responseJSON, null, 2)));
    },
    "password": '<?=$pass;?>',
    "username": '<?=$user;?>',
    "type": "PATCH",
    "data": {
        "name" : "awesome_field",
        "attributes" : {
            "style" : {
                "color" : "blue"
            }
        }
    }
});
<?php
$js = ob_get_clean();
?>
<div class="col-md-12">
<h2>Named Field PATCH Example</h2>
<p>Below is an example <code>PATCH</code> request to <code><?=$url;?></code>.  Notice that 
the entire attributes array was replaced with the submitted information.  Since 
the "type" attribute was not included, it was removed.  This changed the field
from a checkbox to an input!</p>
<p>Before version 1.0, this will be updated to be more granular.  For now, only 
base-level config items can be targetted, and they will be replaced wholesale. For fields,
those items are:</p>
<ul>
    <li>after</li>
    <li>attributes</li>
    <li>before</li>
    <li>content</li>
    <li>field</li>
    <li>label</li>
    <li>placement</li>
    <li>pre_field_content</li>
    <li>post_field_content</li>
</ul>
<p>The same URI accepts the <code>PUT</code> verb.  <code>PUT</code> requests 
are idempotent.  They should contain all of the field's information, just like a
<code>POST</code> request made to the resource type's base URI.</p>
</div>
<div class="col-md-6">
    <h3>Request</h3>
    <pre><code>
    <?=Html::entities($js);?>
    </code></pre>
</div>
<div class="col-md-6">
    <h3>Response</h3>
    <pre><code id="test_4_response"><i class="fa fa-spinner fa-spin"></i></code></pre>
    <p id="test_4_status"></p>
    <h4>Rendered HTML</h4>
    <pre><code id="test_4_html"><i class="fa fa-spinner fa-spin"></i></code></pre>
</div>
<script>
    function test_4() {
        <?=$js;?>
    }
</script>
</div>
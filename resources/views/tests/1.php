<div class="row"><?php
$url = url('api/v.9/fields');
ob_start();
?>$.ajax({
    "url": "<?=Html::entities($url);?>",
    "complete": function(jqXHR, status){
        $('#test_1_status').empty().append('<code>' + jqXHR.status + ' ' + jqXHR.statusText + '</code>');
        prep_2(); // prep next test
    },
    "error": function(jqXHR, status, error) {
        $('#test_1_html').empty().append(JSON.stringify(jqXHR.responseJSON, null, 2));
    },
    "success": function(data, status, jqXHR) {
        $('#test_1_html').empty().append(data.html);
        $('#test_1_response').empty().append(escapeHtml(JSON.stringify(jqXHR.responseJSON, null, 2)));
    },
    "password": '<?=$pass;?>',
    "username": '<?=$user;?>',
    "type": "GET",
    "data": {
        "field": "input",
        "attributes" : {
            "type" : "text",
            "value": "hello world",
            "class" : ["test", "test2"],
            "id" : "whatever",
            "selected" : "selected",
            "name" : "field_name",
            "data" : {
                "some_data_field" : "some value here"
            }
        },
        "before" : "<div class='example_container'>",
        "after" : "</div>"
    }
});
<?php
$js = ob_get_clean();
?>
<div class="col-md-12">
<h2>Unnamed Field GET Example</h2>
<p>Below is an example <code>GET</code> request to <code><?=$url;?></code>.  The API takes 
the submitted configuration and returns an HTML rendering of it.  The same request
made with a "name" parameter and with the <code>POST</code> verb would store the field for later
retrieval.</p>
</div>
<div class="col-md-6">
    <h3>Request</h3>
    <pre><code>
    <?=Html::entities($js);?>
    </code></pre>
</div>
<div class="col-md-6">
    <h3>Response</h3>
    <pre><code id="test_1_response"><i class="fa fa-spinner fa-spin"></i></code></pre>
    <p id="test_1_status">&nbsp;</p>
    <h4>Rendered HTML</h4>
    <pre><code id="test_1_html"><i class="fa fa-spinner fa-spin"></i></code></pre>
</div>
<script>
    function test_1() {
        <?=$js;?>
    }
    function prep_2() {
        $.ajax({
            "url": "<?=Html::entities($url);?>/awesome_field",
            "complete": function(jqXHR, status){
                test_2(); // call next test
            },
            "error": function(jqXHR, status, error) {
            },
            "success": function(data, status, jqXHR) {
            },
            "password": '<?=$pass;?>',
            "username": '<?=$user;?>',
            "type": "DELETE",
            "data": {
                "name" : "awesome_field"
            }
        });
    }
</script>
</div>
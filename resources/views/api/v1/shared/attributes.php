<?php
$attributes = [];
// deal with special cases first

// data-*
if (array_key_exists('data', $input_atts) && is_array($input_atts['data']) && $input_atts['data']) {
    foreach($input_atts['data'] as $data=>$val) {
        $attributes [] = 'data-' . Html::entities($data) . '="' . Html::entities($val) . '"';
    }
    unset($input_atts['data']);
}

// style
if (array_key_exists('style', $input_atts) && is_array($input_atts['style']) && $input_atts['style']) {
    $styles = [];
    foreach($input_atts['style'] as $selector=>$style) {
        $styles [] = "$selector:$style";
    }
    $attributes [] = 'style="' . Html::entities(implode(';', $styles)) . '"';
    unset($input_atts['style']);
}

// everything else
foreach($attribute_list as $att => $strategy) {
    if(array_key_exists($att, $input_atts)) {
        switch (true) {
            case ($strategy === NULL && is_string($input_atts[$att])):
                $attributes[] = Html::entities($att) . '="' . Html::entities($input_atts[$att]) . '"';
            break;
            case (is_array($strategy) && is_string($input_atts[$att])):
                $test = mb_strtolower($input_atts[$att]);
                if (in_array($test, $strategy, $strict = true)) {
                    $attributes[] = Html::entities($att) . '="' . Html::entities($test) . '"';
                }
            break;
            case (is_string($strategy) && is_array($input_atts[$att])):
                
                $attributes[] = Html::entities($att) . '="' . implode($strategy, $input_atts[$att]) . '"';
            break;
            case ($strategy === true && $input_atts[$att] == true):
                $attributes[] = Html::entities($att) . '="'. Html::entities($att) .'"';
            break;
        }
        unset($input_atts[$att]);
    }
}

// any atts left in $input_atts at this point will not be rendered
// TODO: add a notice to response if unrendered (and potentially non-standards compliant) atts exist

echo implode(' ', $attributes);

<?php
switch($config['placement']) {
    case 'wrap':?>
        <label <?=$attributes;?>><?=$config['pre_field_content'], $html, $config['post_field_content'];?></label>
    <?php break;
    case 'before':?>
        <label <?=$attributes;?>><?=$config['content'];?></label><?=$html;?>
    <?php break;
    case 'after':?>
        <?=$html;?><label <?=$attributes;?>><?=$config['content'];?></label>
    <?php break;
}
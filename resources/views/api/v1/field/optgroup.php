<optgroup <?=$attributes;?>><?php
    if (isset($config['children']) && is_array($config['children'])) {
        foreach($config['children'] as $field) {
            echo ApiV1Field::create($field);
        }
    }
?></optgroup>
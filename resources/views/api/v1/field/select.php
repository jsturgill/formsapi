<select <?=$attributes;?>><?php
    if (isset($config['child_fields']) && is_array($config['child_fields'])) {
        foreach($config['child_fields'] as $field) {
            echo ApiV1Field::render($field);
        }
    }
?></select>

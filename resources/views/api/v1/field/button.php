<button <?=$attributes;?>><?=
    // content is not escaped to allow "phrasing content"
    // see https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories#Phrasing_content
    // and http://www.w3.org/html/wg/drafts/html/master/#phrasing-content
    ($config['content']) ? $config['content'] : '';
?></button>
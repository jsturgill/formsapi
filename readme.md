## Forms API
The forms API is an experiment put together in just a few days. There are two resources/endpoints: fields and forms. It is a work in progress.

The API currently relies on HTTP Basic Authentication — and I haven't picked up a certificate for the domain. That means all requests, including credentials, are made IN THE CLEAR.

In its current state, this is a toy only. Don't use it for anything.

See it in action at http://formsapi.jeremiahsturgill.com
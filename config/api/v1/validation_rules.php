<?php
return [
	// Form routes
	'FORM_DELETE_REQUEST' => [
        'name'  => 'required|string|min:1|exists:forms,name,user_id,{USERID}',
	],
	'FORM_INDEX_REQUEST' => [
        'name'  => 'sometimes|string|min:1|exists:forms,name,user_id,{USERID}',
	],
	'FORM_SHOW_REQUEST' => [
        'name'  => 'required|string|min:1|exists:forms,name,user_id,{USERID}',
	],
    'FORM_STORE_REQUEST' => [
        'name'  => 'required|string|min:1|unique:forms,name,NULL,id,user_id,{USERID}',
	],
	'FORM_UPDATE_REQUEST' => [
        'old_name'  => 'sometimes|required|string|min:1|exists:forms,name,user_id,{USERID}',
        'name'      => 'required|string|min:1', // manipulated further in controller
	],
	// Field routes
	'FIELD_DELETE_REQUEST' => [
        'name'  => 'required|string|min:1|exists:fields,name,user_id,{USERID}',
	],
	'FIELD_INDEX_REQUEST' => [
		'field' => 'required_without:name|alpha',
        'name'  => 'required_without:field|string|min:1|exists:fields,name,user_id,{USERID}',
	],
	'FIELD_SHOW_REQUEST' => [
        'name'  => 'required|string|min:1|exists:fields,name,user_id,{USERID}',
	],
	'FIELD_STORE_REQUEST' => [
        'name'  => 'required|string|min:1|unique:fields,name,NULL,id,user_id,{USERID}',
		'field' => 'required|alpha'
	],
	'FIELD_UPDATE_REQUEST' => [
        'old_name'  => 'sometimes|required|string|min:1|exists:fields,name,user_id,{USERID}',
        'name'      => 'required|string|min:1', // manipulated further in controller
	],
];
<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FormsApiServiceProvider extends ServiceProvider {
    
    /**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
		    'apiv1field', function(){
		        return new \App\Library\Api\V1\Resources\Field;
		    }
		);
		$this->app->bind(
		    'apiv1form', function(){
		        return new \App\Library\Api\V1\Resources\Form;
		    }
		);
		$this->app->bind(
		    'apiv1dataservice', function() {
		        return new \App\Library\Api\V1\DataService;
		    }
	    );
	    $this->app->bind(
		    'user', function() {
		        return new \App\Library\Api\V1\Models\User;
		    }
	    );
	}
    
}
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['uses'=>'WelcomeController@index']);

Route::get('home', ['uses'=>'HomeController@index']);

//Route::get('test', 'HomeController@test');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::resource('api/v.9/fields', 'Api\V1\FieldsController');
Route::resource('api/v.9/forms', 'Api\V1\FormsController');

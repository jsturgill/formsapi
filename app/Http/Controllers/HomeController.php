<?php namespace App\Http\Controllers;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('home');
	}
	
	public function test()
	{
	    $field = new \App\Library\Api\V1\Models\Field;
	    $field->name = "test";
	    $field->config = "Some JSON here";
	    $field->user_id = 1;
	    $field->minor_version = 0;
	    $field->major_version = 1;
	    try {
    	    $field->save();
    	    echo "<p>Saved field as ID $field->id</p>";
	    } catch (\Illuminate\Database\QueryException $e){
	        echo "Query Exception";
	    } catch (\Exception $e) {
	        echo "Whoops.<pre>{$e->getMessage()}";
	    }
	    
	}

}

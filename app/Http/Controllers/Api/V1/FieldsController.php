<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests;


use App\Library\Api\V1\Validators\Fields\DestroyRequest;
use App\Library\Api\V1\Validators\Fields\IndexRequest;
use App\Library\Api\V1\Validators\Fields\ShowRequest;
use App\Library\Api\V1\Validators\Fields\StoreRequest;
use App\Library\Api\V1\Validators\Fields\UpdateRequest;

use ApiV1DataService;
use ApiV1Field;
use Auth;
use Config;
use Html;
use Input;
use Request;
use Route;


class FieldsController extends Controller {
    use \App\Library\Api\V1\Traits\ResourceController;
    /*
	|--------------------------------------------------------------------------
	| Fields Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles requests to the fields API resource.
	|
	*/
	
	public function __construct() {
    	$this->middleware('auth.oncebasic'); // require basic auth
	}
	
	/**
	 * Display a formatted field.
	 *
	 * @param FieldsRequest 
	 * @return Response Returns a JSON response
	 */
	public function index(IndexRequest $request)
	{
        $input = Request::all();
        try {
            $html = ApiV1Field::loadValues($input)->render();
        } catch (\Exception $e) {
            $html = false;
        }
        switch (true) {
            case (isset($input['name'])) : // if named, fetch
                try {
                    $field = ApiV1DataService::getField($input['name']);
                    $html = $field->render();
                    if ($html) {
                        $json = $this->createSuccessResponseArray(['html'=>$html]);
                        $code = 200; // OK
                    } else {
                        $json = $this->createErrorResponseArray('submitted field not found');
                        $code = 404; // not found
                    }
                } catch (\Exception $e) {
                    $json = $this->createErrorResponseArray($e->getMessage());
                    $code = 404; // not found
                }
            break;
            case ($html) : // render if not named
                $json = $this->createSuccessResponseArray(['html' => $html]);
                $status = 200; // ok
            break;
            default:
                $json = $this->createErrorResponseArray('submitted field not recognized');
                $status = 404; // not found    
            break;
        }
		return response()->json($json, $status);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * Not implemented
	 * 
	 * @return Response
	 */
	public function create()
	{
		$json = $this->createErrorResponseArray('not implemented');
        $status = 501; // not implemented
        return response()->json($json, $status);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(StoreRequest $request)
	{
		$input = Request::all();
		try {
		    $field = ApiV1Field::loadValues($input);
            $field = ApiV1DataService::storeField($field);
            $json = $this->createSuccessResponseArray([
                'name'      => $field->name,
                'config'    => $field->config,
                'html'      => $field->render()
            ]);
            $status = 201; // created 
		} catch (\Exception $e) {
		    $json = $this->createErrorResponseArray($e->getMessage());
            $status = 422; // unprocessable
		}
		return response()->json($json, $status);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param Request
	 * @param  string  $name
	 * @return Response
	 */
	public function show(ShowRequest $request, $name)
	{
	    $json = [];
	    $input = Request::all();
	    if ($input['name'] !== $name) {
	        $json = $this->createErrorResponseArray('resource name in URL does not match "name" createErrorResponseArray');
	        $code = 422; // unprocessable
	    } else {
	        try {
        	    $field = ApiV1DataService::getField($name);
        	    $html = $field->render();
        	    if ($html) {
        	        $json = $this->createSuccessResponseArray(['html'=>$html]);
        	        $code = 200; // OK
                } else {
                    $json = $this->createErrorResponseArray('submitted field not found');
                    $code = 404; // not found
        	    }
	        } catch (\Exception $e) {
	            $json = $this->createErrorResponseArray($e->getMessage());
                $code = 404; // not found
	        }
	    }
		return response()->json($json, $code);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * Not implemented
	 * 
	 * @param  string  $name
	 * @return Response
	 */
	public function edit($name)
	{
		$json = $this->createErrorResponseArray('not implemented');
        $status = 501; // not implemented
        return response()->json($json, $status);
	}

	/**
	 * Update the specified resource in storage.  If a PUT request, the resource
	 * will be created if it does not already exist.  If it exists, the resource
	 * will be updated.
	 *
	 * @param Request $request
	 * @param  string  $name
	 * @return Response
	 */
	public function update(UpdateRequest $request, $name)
	{
	    // TODO:  seperate out some of this mess so that it's easier to follow
        $input = Request::all();
        //check for invalid names
        if ($input['name'] !== $name && @$input['old_name'] !== $name) {
            $json = $this->createErrorResponseArray('resource name in URL does not match name in data');
            $code = 422; // unprocessable
        } elseif (Request::method() == 'PUT' && 
            ($input['name'] !== $name || @$input['old_name'])) {
            // PUT must be idempotent; changing the name is not allowed    
            $json = $this->createErrorResponseArray('PUT requests cannot alter names (no "old_name" ' . 
                'value can be submitted, and the submitted "name" must match the URL');
            $code = 422; // unprocessable
        } else {
            if (Request::method() == 'PUT') { // update or create
                try {
                    $field = ApiV1DataService::getField($name);
                    if ($field) {
                        $field = ApiV1DataService::updateField($field, $input);
                    } else {
                	    $field = ApiV1Field::loadValues($input);
                	    $field = ApiV1DataService::storeField($field);
                	    $json = $this->createSuccessResponseArray([
                	        'html'      => $field->render(),
                            'config'    => $field->config,
                            'name'      => $field->name,
                        ]);
                        $code = 201; // created 
                    }
            	} catch (\Exception $e) {
                    $json = $this->createErrorResponseArray($e->getMessage());
                    $code = 422; // unprocessable
            	}
            } else {
                try {
                    $field = ApiV1DataService::updateField(ApiV1DataService::getField($name), $input);
                } catch (\Exception $e) {
                    $json = $this->createErrorResponseArray($e->getMessage());
                    $code = 422; // unprocessable
                }
            }
            if (!isset($json) &&  $field && $field->render()) {
                $json = $this->createSuccessResponseArray([
                    'html'      => $field->render(),
                    'config'    => $field->config,
                    'name'      => $field->name,
                ]);
                $code = 200; // ok
            } elseif(!$json) {
                $json = $this->createErrorResponseArray('unable to update field');
                $code = 422; // unprocessable
            }
        }
		return response()->json($json, $code);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Request $request
	 * @param  string  $name
	 * @return Response
	 */
	public function destroy(DestroyRequest $request, $name)
	{
		$input = Request::all();
		if ($input['name'] !== $name) {
		    $json = $this->createErrorResponseArray('resource name in URL does not match "name" submitted');
		    $code = 422; // unprocessable
		} else {
            $result = ApiV1DataService::deleteField($name);
            if (is_string($result)) {
                $json = $this->createSuccessResponseArray([
                    'name' => $result
                ]);
                $code = 204; // no content -- a database entry was found, but it
                             // was invalid for some reason and a field object
                             // could not be created!
            } elseif($result instanceof \App\Library\Api\V1\Resources\Field) {
                $json = $this->createSuccessResponseArray([
                    'name'      => $result->name,
                    'config'    => $result->config,
                    'html'      => $result->render(),
                ]);
                $code = 200; // ok
            } else {
                $json = $this->createErrorResponseArray('unknown error');
                $code = 500; // unknown error 
                error_log('A field resource was unsuccessfully deleted: ' . $name . ', owned by user ' . Auth::user()->id );
            }
		}
		return response()->json($json, $code);
	}
}
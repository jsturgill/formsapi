<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Library\Api\V1\Validators\Forms\DestroyRequest;
use App\Library\Api\V1\Validators\Forms\IndexRequest;
use App\Library\Api\V1\Validators\Forms\ShowRequest;
use App\Library\Api\V1\Validators\Forms\StoreRequest;
use App\Library\Api\V1\Validators\Forms\UpdateRequest;

use ApiV1DataService;
use ApiV1Form;
use Auth;
use Config;
use Html;
use Request;

class FormsController extends Controller {
    use \App\Library\Api\V1\Traits\ResourceController;
    /*
	|--------------------------------------------------------------------------
	| Forms Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles requests to the forms API resource.
	|
	*/
	
	public function __construct() {
    	$this->middleware('auth.oncebasic'); // require basic auth
	}
	
	/**
	 * Display a form.
	 *
	 * @return Response Returns a JSON response
	 */
	public function index(IndexRequest $request)
	{
        $input = Request::all();
        try {
            $html = ApiV1Form::loadValues($input)->render();
        } catch (\Exception $e) {
            $json = $this->createErrorResponseArray($e->getMessage());
            $status = 422; // unprocessable
        }
            switch (true) {
                case (!isset($json) && $html):
                    $json = $this->createSuccessResponseArray(['html' => $html]);
                    $status = 200;
                break;
                case (!isset($json)) :
                    $json = $this->createErrorResponseArray('invalid request');
                    $status = 422; // unprocessable
                break;
            }
		return response()->json($json, $status);
	}

	/**
	 * Show the form for creating a new resource.
	 * 
	 * Not implemented.
	 *
	 * @return Response
	 */
	public function create()
	{
		$json = $this->createErrorResponseArray('not implemented');
        $status = 501; // not implemented
        return response()->json($json, $status);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(StoreRequest $request)
	{
		$input = Request::all();
		try {
		    $children = [];
		    $form = ApiV1Form::loadValues($input);
            $new_form = ApiV1DataService::storeForm($form);
            $json = $this->createSuccessResponseArray([
                'name'      => $new_form->name,
                'config'    => $new_form->config,
                'html'      => $new_form->render()
            ]);
            $status = 201; // created 
		} catch (\Exception $e) {
		    $json = $this->createErrorResponseArray($e->getMessage());
            $status = 422; // unprocessable
		}
		return response()->json($json, $status);
	}

	/**
	 * Display the specified resource.
	 * @param Request
	 * @param  int  $id
	 * @return Response
	 */
	public function show(ShowRequest $request, $name)
	{
		$json = [];
        $input = Request::all();
        if ($input['name'] !== $name) {
            $json = $this->createErrorResponseArray('resource name in URL does not match "name" createErrorResponseArray');
	        $code = 422; // unprocessable
        } else {
            try {
                $form = ApiV1DataService::getForm($name);
                $html = $form->render();
                if ($html) {
                    $json = $this->createSuccessResponseArray(['html'=>$html]);
                    $code = 200; // OK
                } else {
                    $json = $this->createErrorResponseArray('submitted form not found');
                    $code = 404; // not found
                }
            } catch (\Exception $e) {
                $json = $this->createErrorResponseArray('resource name in URL does not match "name" createErrorResponseArray');
                $code = 422; // unprocessable
            }
        }
		return response()->json($json, $code);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * Not implemented
	 * 
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$json = $this->createErrorResponseArray('not implemented');
        $status = 501; // not implemented
        return response()->json($json, $status);
	}

	/**
	 * Update the specified resource in storage.  If a PUT request, the resource
	 * will be created if it does not already exist.  If it exists, the resource
	 * will be updated.
	 *
	 * @param Request $request
	 * @param  string  $name
	 * @return Response
	 */
	public function update(UpdateRequest $request, $name)
	{
	    // TODO:  seperate out some of this mess so that it's easier to follow
		$input = Request::all();
        //check for invalid names
        if ($input['name'] !== $name && @$input['old_name'] !== $name) {
            $json = $this->createErrorResponseArray('resource name in URL does not match name in data');
            $code = 422; // unprocessable
        } elseif (Request::method() == 'PUT' && 
            ($input['name'] !== $name || @$input['old_name'])) {
            // PUT must be idempotent; changing the name is not allowed    
            $json = $this->createErrorResponseArray('PUT requests cannot alter names (no "old_name" ' . 
                'value can be submitted, and the submitted "name" must match the URL');
            $code = 422; // unprocessable
        } else {
            if (Request::method() == 'PUT') { // update or create
                try {
                    $form = ApiV1DataService::getForm($name);
                    if ($form) {
                        $form = ApiV1DataService::updateForm($form, $input);
                    } else {
                	    $form = ApiV1Form::loadValues($input);
                	    $form = ApiV1DataService::storeForm($form);
                	    $json = $this->createSuccessResponseArray([
                	        'html'      => $form->render(),
                            'config'    => $form->config,
                            'name'      => $form->name,
                        ]);
                        $code = 201; // created 
                    }
            	} catch (\Exception $e) {
                    $json = $this->createErrorResponseArray($e->getMessage());
                    $code = 422; // unprocessable
            	}
            } else { // only update if not PUT
                try {
                    $form = ApiV1DataService::getForm($name);
                    if ($form) {
                        $form = ApiV1DataService::updateForm($form, $input);
                    } else {
                        $json = $this->createErrorResponseArray('the resource no longer exists');
                        $code = 422; // unprocessable
                    }
                } catch (\Exception $e) {
                    $json = $this->createErrorResponseArray($e->getMessage());
                    $code = 422; // unprocessable
                }
            }
            if (!isset($json) && isset($form) && is_object($form)) { // set a good response if we have a $form object and no response already made
                
                $json = $this->createSuccessResponseArray([
                    'html'      => $form->render(),
                    'config'    => $form->config,
                    'name'      => $form->name,
                ]);
                $code = 200; // ok
            } elseif(!isset($json)) { // set a failure response if no $form object was created and no error was thrown
                $json = $this->createErrorResponseArray('unable to update form');
                $code = 422; // unprocessable
            }
        }
		return response()->json($json, $code);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Request $request
	 * @param  string  $name
	 * @return Response
	 */
	public function destroy(DestroyRequest $request, $name)
	{
		$input = Request::all();
		if ($input['name'] !== $name) {
		    $json = $this->createErrorResponseArray('resource name in URL does not match "name" submitted');
		    $code = 422; // unprocessable
		} else {
            $result = ApiV1DataService::deleteForm($name);
            if (is_string($result)) {
                $json = $this->createSuccessResponseArray([
                    'name' => $result
                ]);
                $code = 204; // no content -- a database entry was found, but it
                             // was invalid for some reason and a field object
                             // could not be created!
            } elseif ($result instanceof \App\Library\Api\V1\Resources\Form) {
                $json = $this->createSuccessResponseArray([
                    'name'      => $result->name,
                    'config'    => $result->config,
                    'html'      => $result->render(),
                ]);
                $code = 200; // ok
            } else {
                $json = $this->createErrorResponseArray('unknown error');
                $code = 500; // unknown error 
                error_log('A field resource was unsuccessfully deleted: ' . $name . ', owned by user ' . Auth::user()->id );
            }
		}
		return response()->json($json, $code);
	}
}
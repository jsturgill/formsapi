<?php namespace App\Library\Api\V1\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
	    if ($this->isApiRoute($request))
		{
			return $this->addCookieToResponse($request, $next($request));
		}

		return parent::handle($request, $next);
	}
	
	protected function isApiRoute($request)  
    {
        $routes = [
                'api/*'
        ];
        $result = false;
        foreach($routes as $route) {
            if ($request->is($route)) {
                $result = true;
            }
        }
            return $result;
    }

}

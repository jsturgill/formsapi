<?php namespace App\Library\Api\V1\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class RequireHttps extends BaseVerifier {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
	    if (!$request->secure()) {
	        $json = ['status' => 'failure', 'error' => 'must use a secure connection'];
	        $status = 403; // forbidden
	        return response()->json($json, $status);
	    }

		return parent::handle($request, $next);
	}
	
}

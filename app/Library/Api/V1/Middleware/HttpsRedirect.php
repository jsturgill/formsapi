<?php namespace App\Library\Api\V1\Middleware;

use Closure;
use Redirect;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class HttpsRedirect extends BaseVerifier {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
	    if (!$request->secure()) {
//            return Redirect::secure($request->path());
	    }

		return parent::handle($request, $next);
	}

}

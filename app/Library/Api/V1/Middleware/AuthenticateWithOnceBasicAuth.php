<?php namespace App\Library\Api\V1\Middleware;
use Auth;
use Closure;
use Config;
use Session;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Routing\Middleware;

class AuthenticateWithOnceBasicAuth implements Middleware {

    /**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;
	
    /**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        return $this->auth->oncebasic() ?: $next($request);
	}
}

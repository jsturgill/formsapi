<?php namespace App\Library\Api\V1\Validators\Fields;

use Auth;
use App\Http\Requests\Request;

class IndexRequest extends Request {

    use \App\Library\Api\V1\Traits\ApiValidator;

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
	    /* could required basic auth like so:
	       Auth::onceBasic();
	       return Auth::check();
	    */
        return true; // auth middleware takes care of protecting the resource
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        return $this->constructRules('FIELD_INDEX_REQUEST');
	}
	
	private function getCustomErrors() {
        return [
        ];
	}


}

<?php namespace App\Library\Api\V1\Validators\Fields;

use App\Http\Requests\Request;
use Auth;
use Config;

class UpdateRequest extends Request {
    
    use \App\Library\Api\V1\Traits\ApiValidator;
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
	    /* could required basic auth like so:
	       Auth::onceBasic();
	       return Auth::check();
	    */
        return true; // auth middleware takes care of protecting the resource
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
	    $rules = $this->constructRules('FIELD_UPDATE_REQUEST');
	    $input = $this->all();
	    if (!isset($input['old_name'])) {
	        $rules['name'] .= 'exists:fields,name,user_id,' . Auth::user()->id;
	    }
	    return $rules;
	}
	
	private function getCustomErrors() {
        return [
            'old_name' => 'the submitted name does not exist in the database'
        ];
	}

}

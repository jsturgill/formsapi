<?php namespace App\Library\Api\V1\Resources;
use Config;
class Field {
    use \App\Library\Api\V1\Traits\Resource;
    use \App\Library\Api\V1\Traits\ElementConstructor;

    public $config;

    public $after, $attributes, $before, $content, $field, $label, $placement, 
            $pre_field_content, $post_field_content;

    /**
     * Stubs for field and label config arrays; will sometimes add keys that are
     * not needed.
     */
    const DEFAULTS = '{
        "after" : null,
        "attributes" : [],
        "before" : null,
        "child_fields" : [],
        "content" : null,
        "field" : null,
        "label" : null,
        "name" : null,
        "placement" : null,
        "pre_field_content" : null,
        "post_field_content" : null
    }';

    /**
     * Checks whether or not the field is valid.
     * 
     * A field is considered valid if it has a template and if its attributes 
     * are defined. These checks are case sensitive.
     * 
     * @param string Field element
     * @return bool Returns true if it is a valid field, false otherwise.
     */
    private function isValidField($field) {
        $result = false;
        if(view()->exists($this->getFieldView($field)) 
            && isset(Config::get('api.v1.ATTRIBUTES')[$field])
        ) {
            $result = true;
        }
        return $result;
    }
    
    /**
     * @return string View path
     */
    private function getFieldView($field = null) {
        $field = ($field) ? $field : $this->field;
        return "api.v1.field.{$field}";
    }
    
    /**
     * @param string $rendering Rendered HTML field
     * @param array $config Array of values
     */
    private function addLabel($rendering, $config) {
        // TODO: add setting to allow generation of IDs for fields, and auto setting of label's for attribute
        $config = $this->setupConfig($config);
        $attributes = $this->getAttributeString('label', $config['attributes']);
        return (string)view($this->getFieldView('label'), ['config' => $config, 
            'html' => $rendering, 'attributes' => $attributes]);
    }
    
    /**
     * Takes the submitted array and attempts to render a field from it
     * @param array $config Submitted or constructed values used to generate field; optional if loadValues already called 
     * @return string|bool HTML rendering of field or FALSE
     */
    public function render($config = null) {
        if (is_array($config)) {
            $this->loadValues($config);
        }
        if (!$this->isValidField($this->field)) {
            throw new \Exception('Attempted to render a field that was not configured:' . $this->field);
        }
        $attributes = $this->getAttributeString();
        $result = (string)view($this->getFieldView(), [ 
            'config'=> [
                'content' => $this->content, 
                'child_fields' => $this->child_fields
            ], 
            'attributes'=>$attributes
        ]);
        if ($result) {
            if ($this->label) {
                $result = $this->addLabel($result, $this->label);
            }
            $result = $this->wrapRendering($result, $this->before, $this->after);
        }
        return $result;
    }
    
    public function loadValues($config) {
        $this->config = $this->setupConfig($config);
        $field = $this->config['field'];
        $result = false;
        $allowed = $this->decodeConstantArray('DEFAULTS');
        if($this->isValidField($field)) {
            foreach($allowed as $att=>$default) {
                if (isset($this->config[$att])) {
                    $this->$att = $this->config[$att];
                } else {
                    $this->$att = $default;
                }
            }
        } else {
            throw new \Exception('Invalid field config: "field" must be specified and a valid element');
        }
        return clone $this;
    }
    
    public function getElement() {
        return $this->field;
    }
    
    public function getAttributes() {
        return $this->attributes;
    }
}
<?php namespace App\Library\Api\V1\Resources;
use Config;
use ApiV1Field;
class Form {
    
    use \App\Library\Api\V1\Traits\Resource;
    use \App\Library\Api\V1\Traits\ElementConstructor;

    public $config;
    public $field = 'form';
    public $after, $attributes, $before, $child_fields, $name;

    /**
     * Stubs for possible form config values
     */
    const DEFAULTS = '{
        "after" : null,
        "attributes" : [],
        "before" : null,
        "child_fields" : [],
        "name" : null
    }';

    /**
     * @return string View path
     */
    private function getFormView() {
        return "api.v1.form.form";
    }
    
    /**
     * Takes the submitted array and attempts to render a form from it
     * @param array $config Submitted or constructed values used to generate form 
     * @return string|bool HTML rendering of field or FALSE
     */
    public function render($config = null) {
        if (is_array($config)) {
            $this->loadValues($config);
        }
        $attributes = $this->getAttributeString();
        $result = (string)view($this->getFormView(), [ 
            'child_fields'  => $this->child_fields,
            'attributes'    => $attributes
        ]);
        if ($result) {
            $result = $this->wrapRendering($result, $this->before, $this->after);
        }
        return $result;
    }
    
    public function loadValues($config) {
        $this->config = $this->setupConfig($config);
        $result = false;
        $allowed = $this->decodeConstantArray('DEFAULTS');
        foreach($allowed as $att=>$null) {
            if (isset($this->config[$att])) {
                $this->$att = $this->config[$att];
            } else {
                $this->$att = null;
            }
        }
        try {
            foreach($this->child_fields as $key=>$child_config) {
                if (!isset($child_config['name'])) {
                    throw new \Exception('form child fields must be named');
                }
                $this->child_fields[$key] = ApiV1Field::loadValues($child_config);
            }
        } catch (\Exception $e) {
            throw new \Exception('invalid child field passed into form: ' . $e->getMessage());
        }
        return clone $this;
    }
    
    public function getElement() {
        return $this->field;
    }
    
    public function getAttributes() {
        return $this->attributes;
    }
}
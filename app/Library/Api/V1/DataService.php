<?php namespace App\Library\Api\V1;

use Auth;
use ApiV1Field;
use ApiV1Form;
use DB;
use User;

class DataService {
    
    private $user;
    
    public function __construct() {
        if (Auth::check()) {
            $this->user = User::where('id', '=', Auth::user()->id)->first();
        } else {
            throw new \Exception('Something attempted to use the API without being logged in.');
        }
    }
    
    public function getField($name) {
        $result = false;
        $field_model = $this->fetchFieldModel($name);
        
        if ($field_model) {
            $config = $this->decodeConfig($field_model->config);
            if ($config) {
                $result = ApiV1Field::loadValues($config);
                $result->name = $name;
            } else {
                throw new \Exception('invalid config');
            }
        }
        return $result;
    }
    
    public function getForm($name) {
        $result = false;
        $form_model = $this->fetchFormModel($name);
        if ($form_model) {
            $config = $this->decodeConfig($form_model->config);
            if ($config) {
                $form = ApiV1Form::loadValues($config);
                $form->name = $name;
            } else {
                throw new \Exception('invalid config');
            }
            $result = $this->loadFormChildFields($form, $form_model);
        }
        return $result;
    }
    
    private function loadFormChildFields(\App\Library\Api\V1\Resources\Form $form, 
        \App\Library\Api\V1\Models\Form $form_model) {
        foreach($form_model->fields()->orderBy('weight', 'ASC')->get() as $field) {
                $form->child_fields[] = ApiV1Field::loadValues($this->decodeConfig($field->config));
            }
        return $form;
    }
    
    public function storeField($field) {
        if (!$field->render()) {
            throw new \Exception('The "field" parameter must be specified');
        }
        $values = [
            'config'    => $this->encodeConfig($field->config), 
            'name'      => $field->name
        ];
        $field_model = $this->createFieldModel($values);
        $field_model = $this->user->fields()->save($field_model);
        return ApiV1Field::loadValues($field->config);;
    }
    
    public function storeForm(\App\Library\Api\V1\Resources\Form $form) {
        $stored_config = $form->config;
        unset($stored_config['child_fields']);
        $values = [
            'config'    => $this->encodeConfig($stored_config),
            'name'      => $form->name
        ];
        $form_model = $this->createFormModel($values);
        $service = $this;
        DB::transaction(function() use ($service, $form_model, $form) {
            $form_model = $service->user->forms()->save($form_model);
            foreach ($form->child_fields as $weight=>$field) {
                $field_model = $service->fetchFieldModel($field->name);
                if ($field_model) {
                    $field_model->config = $service->encodeConfig($field->config);
                    $field_model->save();
                } else {
                    $values = [
                        'config'    => $service->encodeConfig($field->config),
                        'name'      => $field->name,
                    ];
                    $field_model = $service->createFieldModel($values);
                    $service->user->fields()->save($field_model);
                }
                $field_model->forms()->attach($form_model, ['weight' => $weight]);
            }
        });
        return $form;
    }
    
    private function fetchFieldModel($name) {
        return $this->user->fields()->where('name', '=', $name)->first();
    }
    
    private function fetchFormModel($name) {
        return $this->user->forms()->where('name', '=', $name)->first();
    }
    
    /**
     * Delete the submitted field.
     * 
     * @param $name Name of the field to be deleted
     * @return bool|ApiV1Field|string Returns false if no match is found, the ApiV1Field object if it is found and has a valid config, and the name of the deleted field if there is a problem with the config but a matching entry was found
     */
    public function deleteField($name) {
        $field_model = $this->fetchFieldModel($name);
        $result = false;
        if ($field_model) {
            $config = $this->decodeConfig($field_model->config);
            try {
                $field = ApiV1Field::loadValues($config);
                if ($field->render()) {
                    $result = $field;
                } else {
                    $result = $field_model->name;
                }
            } catch (\Exception $e) {
                $result = $field_model->name;
            }
            $field_model->forms()->detach();
            $field_model->delete();
        }
        return $result;
    }
    
    /**
     * Delete form.
     * 
     * @param $name Name of the form to be deleted
     * @return bool|ApiV1Field|string Returns false if no match is found, the ApiV1Field object if it is found and has a valid config, and the name of the deleted field if there is a problem with the config but a matching entry was found
     */
    public function deleteForm($name) {
        $form_model = $this->fetchFormModel($name);
        $result = false;
        if ($form_model) {
            try {
                $form = $this->getForm($name);
                if ($form->render()) {
                    $result = $form;
                } else {
                    $result = $name;
                }
            } catch (\Exception $e) {
                $result = $name;
            }
            $form_model->fields()->detach(); // clear associations table
            $form_model->delete(); // then delete
        }
        return $result;
    }
    
    public function updateField($field, array $patch_config) {
        $result = false;
        $new_config = $this->mergeConfigs($field->config, $patch_config);
        $field_model = $this->fetchFieldModel($field->name);
        if ($field_model) {
            $field_model->config = $this->encodeConfig($new_config);
            $field_model->name = $new_config['name'];
            try {
                $field_model->save();
                $result = ApiV1Field::loadValues($new_config);
            } catch (\Exception $e) {
                throw new \Exception('new name "' . $field_model->name . 
                    '" already exists');
            }
        }
        return $result;
    }
    
    public function updateForm($form, array $patch_config) {
        $result = false;
        $child_fields = (isset($patch_config['child_fields'])) ? $patch_config['child_fields'] : false;
        $new_config = $this->mergeConfigs($form->config, $patch_config);
        $new_config['name'] == (isset($new_config['name'])) ? $new_config['name'] : $form->name;
        $form_model = $this->fetchFormModel($form->name);
        if ($form_model) {
            $service = $this;
            DB::transaction(function() use ($service, $form_model, $new_config, $child_fields, &$result) {
                if ($child_fields) {
                    unset($new_config['child_fields']);
                    $form_model->fields()->detach();
                    foreach($child_fields as $weight=>$field_config) {
                        // The child fields are not updated or altered; only the
                        // associations table is updated.  To alter the child fields,
                        // the consumer should act on the field resource.
                        // (due to time constraints if nothing else)
                        if (!isset($field_config['name'])) {
                            throw new \Exception('child fields must have names');
                        }
                        $field_model = $service->fetchFieldModel($field_config['name']);
                        if ($field_model) {
                            $field_model->forms()->attach($form_model, ['weight' => $weight]);
                        }
                    }
                }
                $form_model->config = $this->encodeConfig($new_config);
                $form_model->name = $new_config['name'];
                try {
                    $form_model->save();
                    $result = $this->getForm($new_config['name']);
                } catch (\Exception $e) {
                    throw new \Exception('new name "' . $form_model->name . 
                        '" already exists');
                }
            });
        }
        return $result;
    }
    
    private function mergeConfigs($old_config, $patch_config) {
        return $new_config = array_merge($old_config, $patch_config); // nothing complicated
    }
    
    private function form($array = []) {
        $form = new \App\Library\Api\V1\Models\Form();
        foreach ($array as $column=>$val) {
            $form->$column = $val;
        }
        return $form;
    }
    
    private function createFieldModel($array = []) {
        $field_model = new \App\Library\Api\V1\Models\Field();
        foreach ($array as $column=>$val) {
            $field_model->$column = $val;
        }
        return $field_model;
    }
    
    private function createFormModel($array = []) {
        $form_model = new \App\Library\Api\V1\Models\Form();
        foreach ($array as $column=>$val) {
            $form_model->$column = $val;
        }
        return $form_model;
    }
    
    private function encodeConfig($config) {
        return base64_encode(json_encode($config));
    }
    
    private function decodeConfig($config) {
        return json_decode(base64_decode($config), $assoc_array = true);
    }
}
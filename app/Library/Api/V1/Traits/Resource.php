<?php namespace App\Library\Api\V1\Traits;

trait Resource {

    /**
	 * Returns the input array with stubbed entries for all base fields that will be
	 * used elsewhere.
	 * 
	 * @param array Array of submitted values
	 * @return array
	 */
	private function setupConfig(array $config){
	    $defaults = $this->decodeConstantArray('DEFAULTS');
	    return array_merge($defaults, $config);
	}
    
    /**
     * Converts class constant string into JSON array.
     * 
     * @param string Name of class constant
     * @result array|bool Returns Array on success, false on failure
     */
    private function decodeConstantArray($constant) {
        $result = false;
        $constant = constant('self::' . $constant);
        if ($constant) {
            $result = json_decode($constant, $assoc_array = true);
        }
        return $result;
    }

}

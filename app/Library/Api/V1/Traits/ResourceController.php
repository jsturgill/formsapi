<?php namespace App\Library\Api\V1\Traits;
use Config;

trait ResourceController {

    /**
     * Create a "failure" response array that will be converted to JSON
     * @param array $additional
     * @return array
     */
    private function createErrorResponseArray($error, array $additional = array()) {
        $array = [
            'status'    => Config::get('api.v1.FAILURE_STATUS'),
            'error'     => $error,
        ];
        return array_merge($additional, $array);
    }
    
    /**
     * Create a "success" response array that will be converted to JSON
     * @param array $additional
     * @return array
     */
    private function createSuccessResponseArray(array $additional = array()) {
        $array = [
            'status'    => Config::get('api.v1.SUCCESS_STATUS'),
        ];
        return array_merge($additional, $array);
    }
}
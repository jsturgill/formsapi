<?php namespace App\Library\Api\V1\Traits;

use Illuminate\Http\JsonResponse;

use Auth;
use Config;

trait ApiValidator {
    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
	public function response(array $errors)
	{
        $response = [
            'status'    => Config::get('api.v1.FAILURE_STATUS'),
            'error'     => 'request failed validation',
            'validation_failures' => $this->updateErrors($errors),
        ];
        
        return new JsonResponse($response, 422);
	}
	
	/**
	 * Replace default error messages with custom ones
	 * 
	 * @errors array $errors Array of $key=>$value pairs
	 */
	private function updateErrors($errors) {
	    $custom = $this->getCustomErrors();
	    foreach($errors as $error=>$message) {
	        if(array_key_exists($error, $custom)) {
	            $errors[$error] = $custom[$error];
	        }
	    }
	    return $errors;
	}
	
	public function constructRules($context) {
	    $rules = Config::get('api.v1.validation_rules.' . $context);
	    if (Auth::check()) {
	        $id = Auth::user()->id;
	    } else {
	        $id = 0;
	    }
	    $replacements = ['{USERID}' => $id];
	    foreach ($rules as $key => $string) {
	        // ghetto mb_str_replace function
	        foreach($replacements as $needle=>$replacement) {
	            if ($replacement !== $needle) {
    	            $start = mb_strpos($string, $needle);
    	            while( $start !== false) {
    	                $len = mb_strlen($needle);
    	                $before = mb_strcut($string, 0, $start);
    	                $after = mb_strcut($string, $start + $len);
    	                $string = $before.$replacement.$after;
    	                $start = mb_strpos($string, $needle);
    	            }
    	            $rules[$key] = $string;
	            }
	        }
	    }
	    return $rules;
	}
}
<?php namespace App\Library\Api\V1\Traits;
use Config;

trait ElementConstructor {
    /**
     * Constructs a list of valid attributes for the given element.
     * 
     * @param string Field element; if ommitted, $this->getElement()
     * @return array Returns an array of valid attributes for the given element; keys are attributes, values indicate the strategy used to generate the attribute string
     */
    private function getAttributeList($element = null) {
        $element = ($element) ? $element : $this->getElement();
        $event_handlers = Config::get('api.v1.GLOBAL_EVENT_HANDLERS');
        $event_handlers = array_flip($event_handlers);
        $global_attributes = Config::get('api.v1.GLOBAL_ATTRIBUTES');
        $attributes = Config::get('api.v1.ATTRIBUTES');
        return array_merge($event_handlers, $global_attributes, $attributes[$element]);
    }
    
    /**
     * @return string View path
     */
    private function getAttributesView() {
        return 'api.v1.shared.attributes';
    }
    
    /**
     * @return string View path
     */
    private function getWrapperView() {
        return 'api.v1.shared.wrap';
    }
    
    /**
     * @param string $rendering HTML rendering of an element
     * @param string $before HTML to go before the element
     * @param string $after HTML to go after the element
     * @return string Rendered element, wrapped (HTML)
     */
    private function wrapRendering($rendering, $before, $after) {
        return (string)view($this->getWrapperView(), ['before'=> $before,
            'after' => $after, 'html' => $rendering]);
    }
    
    /**
     * @param string $element Field element; if ommitted, $this->getElement()
     * @param array $input_atts Array of settings; if ommitted, $this->getAttributes()
     * @return string Returns an attribute string
     */
    private function getAttributeString($element = null, $input_atts = null) {
        $element = ($element) ? $element : $this->getElement();
        $input_atts = ($input_atts) ? $input_atts : $this->getAttributes();

        $attribute_list = $this->getAttributeList($element);
        return (string)view($this->getAttributesView($element), 
            ['field'=>$element, 'input_atts' => $input_atts, 
            'attribute_list' => $attribute_list]);
    }
}
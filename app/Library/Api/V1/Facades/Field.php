<?php namespace App\Library\Api\V1\Facades;

use \Illuminate\Support\Facades\Facade;

class Field extends Facade {

    protected static function getFacadeAccessor() { return 'apiv1field'; }

}

<?php namespace App\Library\Api\V1\Facades;

use \Illuminate\Support\Facades\Facade;

class Form extends Facade {

    protected static function getFacadeAccessor() { return 'apiv1form'; }

}

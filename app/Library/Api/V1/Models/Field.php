<?php namespace App\Library\Api\V1\Models;

class Field extends \Illuminate\Database\Eloquent\Model {
    protected $guarded = ['*'];
    
    public function user() {
        return $this->belongsTo('App\Library\Api\V1\Models\User');
    }
    
    public function forms()
    {
        return $this->belongsToMany('App\Library\Api\V1\Models\Form');
    }
}